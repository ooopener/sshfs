#!/bin/ash -x
# #!/usr/bin/env sh

# To debug, use this shebang: #!/bin/ash -x

REMOTE_DIR=${REMOTE_DIR:-/}
REMOTE_SERVER=${REMOTE_SERVER:-127.0.0.1}
SSH_KEY=${SSH_KEY}
SSH_USER=${SSH_USER:-root}
SSH_PORT=${SSH_PORT:-22}

MOUNT=/mnt

# sync and unmount on shutdown
trap cleanup SIGHUP SIGINT SIGQUIT SIGABRT SIGTERM

cleanup() {
    echo -n "Caught signal. Cleaning up now... "
    sync
    umount -vl ${MOUNT}
    echo "Done!"
    exit 0
}

mkdir -p /root/.ssh

echo -----BEGIN OPENSSH PRIVATE KEY----- > /root/.ssh/server_key
echo ${SSH_KEY} | sed 's/ /\n/g' >> /root/.ssh/server_key
echo -----END OPENSSH PRIVATE KEY----- >> /root/.ssh/server_key

config=$(cat <<EOF
Host ${REMOTE_SERVER}
Port ${SSH_PORT}
IdentityFile /root/.ssh/server_key
StrictHostKeyChecking no
EOF
)

echo "$config" > /root/.ssh/config

chmod -R 600 /root/.ssh

echo '--- launch sshfs ---'

sshfs -o allow_other ${SSH_USER}@${REMOTE_SERVER}:${REMOTE_DIR} ${MOUNT}
