[![](https://images.microbadger.com/badges/version/ooopener/sshfs.svg)](https://microbadger.com/images/ooopener/sshfs "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/ooopener/sshfs.svg)](https://microbadger.com/images/ooopener/sshfs "Get your own image badge on microbadger.com") [![](https://images.microbadger.com/badges/commit/ooopener/sshfs.svg)](https://microbadger.com/images/ooopener/sshfs "Get your own commit badge on microbadger.com") [![](https://images.microbadger.com/badges/license/ooopener/sshfs.svg)](https://microbadger.com/images/ooopener/sshfs "Get your own license badge on microbadger.com")
[![](https://img.shields.io/docker/pulls/ooopener/sshfs.svg)](https://img.shields.io/docker/pulls/ooopener/sshfs.svg)

  
sshfs
==================

a sshfs image - built on [alpine:latest](https://hub.docker.com/r/alpine).

Versions
--------

* [latest](https://bitbucket.org/ooopener/sshfs/src/master/Dockerfile) ( alpine:latest ) [![](https://images.microbadger.com/badges/image/ooopener/sshfs.svg)](https://microbadger.com/images/ooopener/sshfs "Get your own image badge on microbadger.com")

How to user
-----------

In order to run SSHFS inside container it requires privileged permissions.

Environment variables:

- REMOTE_DIR
    - The remote directory
- REMOTE_SERVER
    - The remote server IP or domain
- SSH_KEY
    - The private key
- SSH_PORT
    - The remote SSH port
- SSH_USER
    - The SSH user

License
-------

See the [LICENSE file](LICENSE) for license text and copyright information.
