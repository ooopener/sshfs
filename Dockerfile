FROM alpine:latest

LABEL maintainer="Benoit Pouzet <benoit@ooopener.com>"

# Build-time metadata as defined at http://label-schema.org
ARG BUILD_DATE
ARG VCS_REF
ARG VERSION
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.name="sshfs" \
      org.label-schema.description="a sshfs image - built on alpine." \
      org.label-schema.url="https://bitbucket.org/ooopener/sshfs" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://bitbucket.org/ooopener/sshfs" \
      org.label-schema.vendor="ooopener" \
      org.label-schema.version=$VERSION \
      org.label-schema.schema-version="1.0" \
      org.label-schema.license="MIT"

ENV REMOTE_DIR /
ENV REMOTE_SERVER 127.0.0.1
ENV SSH_KEY key
ENV SSH_USER root
ENV SSH_PORT 22

RUN apk add --update \
    sshfs \
  && rm -rf /var/cache/apk/*

ENTRYPOINT ["/entrypoint.sh"]
